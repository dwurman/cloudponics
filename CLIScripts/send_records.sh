#!/bin/sh

#curl -F records=@/mnt/sda1/records.txt cloudponics-dashdrive.rhcloud.com/records

#curl --upload-file /Users/dwurman/WebProjects/cloudponics/CLIScripts/records.txt http://localhost:3000/api/records

#curl -4 -X POST --data record="$line" http://localhost:3000/api/records

counter=1;

while read line
do
    response=$(curl --silent -4 -X POST --data record="$line" http://localhost:3000/api/records)
    if [ "$response" == "OK" ]; then
#        sed "$line" /Users/dwurman/WebProjects/cloudponics/CLIScripts/records.txt
        sed -i "$counter"d /Users/dwurman/WebProjects/cloudponics/CLIScripts/records.txt
    fi
    ((counter+=1))
done < /Users/dwurman/WebProjects/cloudponics/CLIScripts/records.txt
