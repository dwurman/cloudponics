/**
 * Created by dwurman on 9/12/14.
 */
'use-strict'

angular.module('cpApp.controllers', []);

angular.module('cpApp.controllers').controller('MainController', ['$http', '$scope',
    function($http, $scope){

        $scope.datalogs = [];
        $scope.predicate = '-timeStamp';
        $scope.messages = [];

        $scope.autoUpdateTable = true;


        $scope.get_params = function(){
            $http.get('/api/params/1').success(function(data, status, headers, config){
                console.log(data)
                $scope.params = data;
            })
        }

        $scope.get_header = function(){
            $http.get('/api/header/1').success(function(data, status, headers, config){
                if(data){
                    $scope.header = data;
                } else {
                    $scope.header = {
                        systemOff: false,
                        updateParams: false,
                        pH4Calibration: false,
                        pH7Calibration: false,
                        eCCalibration: false,
                        n1PumpOR: false,
                        n2PumpOR: false,
                        n3PumpOR: false,
                        n4PumpOR: false,
                        pHDownPumpOR: false,
                        optProcedureOR: false
                    }
                }
            })
        }

        $scope.toggleSwitch = function(name) {
            if($scope.header[name] == true){
                $scope.header[name] = false;
            }
            else if ($scope.header[name] == false) {
                $scope.header[name] = true;
            }
            $scope.saveHeader(function(err, header) {
                if(err){
                    console.log(err);
                }
            })
        }

        var autoUpdateInterval;

        $scope.toggleAutoUpdateTable = function() {
            if($scope.autoUpdateTable) {
                console.log("Clearing interval")
                $scope.autoUpdateTable = false;
                window.clearInterval(autoUpdateInterval);
            } else if (!$scope.autoUpdateTable) {
                autoUpdateInterval = setInterval(function(){
                    $scope.getData();
                    $scope.get_header();
                }, 2000);
                console.log("Setting interval")
                $scope.autoUpdateTable = true;
                window.setInterval(autoUpdateInterval);
            }
        }

        $scope.toggleAutoUpdateTable();


        $scope.get_params();
        $scope.get_header();


        $scope.getData = function(){
            $http.get('/api/datalog/1').success(function(data, status, headers, config){
                console.log(data)
                data.forEach(function(el, index){
                    el.index = index
                })
                $scope.datalogs = data;
            })
        }

        $scope.saveHeader = function(callback){
            $http.post('/api/header/1', $scope.header).success(function(data, status, headers, config){
                callback(null, data);
            }).error(function(err){callback(err)})
        }

        $scope.removeAll = function(){
            $http.get('/api/removeAll/1').success(function(data, status, headers, config){
                console.log(data);
            })
        }

        $scope.updateParamsFunc = function(params){
            console.log(params);
            $http.post('/api/params/1', params).success(function(data, statis, headers, config){
                $scope.params = data
            }).error(function(err){console.log(err)})
        }

        $scope.sendPost = function(){
            console.log("Sending post;")

            $http.post('/api/timestamp/', {timestamp: $scope.timeStamp}).success(function (data, status, headers, config){
                $scope.postStatus = "TODO OK!"
                console.log(data)
            }).error(function(data, status, headers, config){
                $scope.postStatus = "ERROR :("
                console.log(data)
            })

        }
    }])

angular.module('cpApp.controllers').controller('MobileController', ['$http', '$scope', function($http, $scope){
    $scope.tab_activo = 1;
    $scope.params = {}
    $scope.status = {}

    var addZero = function(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    var autoUpdateInterval = setInterval(function(){
        $scope.getStatus();
    },2000);

    $scope.getStatus = function(){
        $http.get('/api/status/1').success(function(data, status, headers, config){
            if(data){
                $scope.status = data;
                $scope.status.eC = parseFloat(data.eC).toFixed(1)
                $scope.status.pH = parseFloat(data.pH).toFixed(1)
                $scope.status.temp = parseFloat(data.temp).toFixed(1)
                $scope.status.airTemp = parseFloat(data.airTemp).toFixed(1)
                $scope.status.humidity = parseFloat(data.humidity).toFixed(0)
                console.log($scope.params);
            }
        }).error(function(err){
            console.log(err)
        })
    }

    $scope.getParams = function(){
        $http.get('/api/params/1').success(function(data, status, headers, config){
            if(data){
                $scope.params = data;
                var lightOn = new Date(0,0,0,data.hourOn, data.minutesOn, 0, 0);
                var lightOff = new Date(0,0,0, data.hourOff, data.minutesOff, 0, 0);
                $scope.params.lightOn = addZero(lightOn.getHours()) + ':' + addZero(lightOn.getMinutes())
                $scope.params.lightOff  = addZero(lightOff.getHours()) + ':' + addZero(lightOff.getMinutes())
                console.log($scope.params.lightOn)
                console.log($scope.params.lightOff)
                $scope.params.hourOn = undefined
                $scope.params.hourOff = undefined
                $scope.params.minutesOn = undefined
                $scope.params.minutesOff = undefined
            }
        }).error(function(err){
            console.log(err)
        })
    }

    $scope.getParams()

    $scope.setParams = function(){

        var params = {}

        if($scope.params.eCGoal) {
            params.eCGoal = $scope.params.eCGoal
        }
        if($scope.params.pHGoal) {
            params.pHGoal = $scope.params.pHGoal
        }
        if($scope.params.humidityGoal) {
            params.humidityGoal = $scope.params.humidityGoal
        }
        if($scope.params.airTempGoal) {
            params.airTempGoal = $scope.params.airTempGoal
        }
        if($scope.params.nut1Prop) {
            params.nut1Prop = $scope.params.nut1Prop
        }
        if($scope.params.nut1Name) {
            params.nut1Name = $scope.params.nut1Name
        }
        if($scope.params.nut2Prop) {
            params.nut2Prop = $scope.params.nut2Prop
        }
        if($scope.params.nut2Name) {
            params.nut2Name = $scope.params.nut2Name
        }
        if($scope.params.nut3Prop) {
            params.nut3Prop = $scope.params.nut3Prop
        }
        if($scope.params.nut3Name) {
            params.nut3Name = $scope.params.nut3Name
        }
        if($scope.params.nut4Prop) {
            params.nut4Prop = $scope.params.nut4Prop
        }
        if($scope.params.nut4Name) {
            params.nut4Name = $scope.params.nut4Name
        }
        if($scope.params.nut5Prop) {
            params.nut5Prop = $scope.params.nut5Prop
        }
        if($scope.params.nut5Name) {
            params.nut5Name = $scope.params.nut5Name
        }
        if($scope.params.nut6Prop) {
            params.nut6Prop = $scope.params.nut6Prop
        }
        if($scope.params.nut6Name) {
            params.nut6Name = $scope.params.nut6Name
        }
        if($scope.params.lightOn){
            params.hourOn = $scope.params.lightOn.split(':')[0]
            params.minutesOn = $scope.params.lightOn.split(':')[1]
            console.log(params.hourOn)
            console.log(params.minutesOn)
        }
        if($scope.params.lightOff){
            params.hourOff = $scope.params.lightOff.split(':')[0]
            params.minutesOff = $scope.params.lightOff.split(':')[1]
            console.log(params.hourOff)
            console.log(params.minutesOff)
        }

        $http.post('/api/params/1', params).success(function(data, status, headers, config){
            $scope.params = data
            var lightOn = new Date(0,0,0,data.hourOn, data.minutesOn, 0, 0);
            var lightOff = new Date(0,0,0, data.hourOff, data.minutesOff, 0, 0);
            $scope.params.lightOn = addZero(lightOn.getHours()) + ':' + addZero(lightOn.getMinutes())
            $scope.params.lightOff  = addZero(lightOff.getHours()) + ':' + addZero(lightOff.getMinutes())
            $scope.params.hourOn = undefined
            $scope.params.hourOff = undefined
            $scope.params.minutesOn = undefined
            $scope.params.minutesOff = undefined
        }).error(function(err){console.log(err)})

    }
}])


