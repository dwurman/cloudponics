'use-strict'

angular.module('cpApp', [
    'ngRoute',
    'cpApp.controllers',
    'ui.bootstrap'
]).config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
    $routeProvider.when('/data', {
        templateUrl: 'partials/data.html',
        controller: 'DataController'
    })
    $routeProvider.when('/config', {
        templateUrl: 'partials/config.html',
        controller: 'ConfigController'
    })
    $routeProvider.otherwise({redirectTo: '/'});
}])