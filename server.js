/**
 * Created by dwurman.
 */
var express = require('express');
var app = express();
var routes = require('./routes')
var bodyParser = require('body-parser');
var async = require('async');
var _ = require('underscore');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//app.disable('x-powered-by');

var port = process.env.OPENSHIFT_NODEJS_PORT || 3000;
var ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

app.use('/', express.static(__dirname + '/public'));

app.use('/api', routes);

var server = app.listen(port, ip_address, function(){
    console.log('Magic happens at port ' + server.address().port);
});
