/**
 * Created by dwurman on 9/12/14.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var moment = require("moment")
var async = require('async');
var _ = require('underscore');
var csv = require('express-csv');

var mongoose_host = process.env.OPENSHIFT_MONGODB_DB_HOST || '127.0.0.1'
var mongoose_port = process.env.OPENSHIFT_MONGODB_DB_PORT || '27017';

//var options = {user: 'admin', pass: 'IbfSf8lsUuqN'}

//var options = {db: 'cloudponics'}

var render_for_arduino = function(object){
    var str = "";
    Object.keys(object).forEach(function(key){
        str += key + "=" + object[key] + ", ";
    })
    return str;
}

if(process.env.OPENSHIFT_APP_NAME == 'cloudponics'){
    console.log("Connected to OpenShift MongoDB Catridge");
    mongoose.connect('mongodb://admin:IbfSf8lsUuqN@' + mongoose_host + ':' + mongoose_port + '/cloudponics');
} else {
    console.log("Connected to local MongoDB");
    mongoose.connect('mongodb://' + mongoose_host + ':' + mongoose_port + '/');
}

//mongoose.connect('mongodb://' + mongoose_host + ':' + mongoose_port + '/' );
var PostedData = mongoose.model('PostedData',
    {
        str: String
    }
)

var Params = mongoose.model('Params',
    {
        arduinoID: Number,
        nut1Prop: Number,
        nut2Prop: Number,
        nut3Prop: Number,
        nut4Prop: Number,
        nut5Prop: Number,
        nut6Prop: Number,
        totalNut: Number,
        pHDownML: Number,
        pHSamplingInterval: Number,
        eCSamplingInterval: Number,
        tempSamplingInterval: Number,
        pHDownPumpActInterval: Number,
        nutPumpActInterval: Number,
        optProcedureInterval: Number,
        pHPrecision: Number,
        eCPrecision: Number,
        pHGoal: Number,
        eCGoal: Number,
        circulationTime: Number,
        humidityGoal: Number,
        airTempGoal: Number,
        hourOn: Number,
        hourOff: Number,
        minutesOn: Number,
        minutesOff: Number,
        nut1Name: String,
        nut2Name: String,
        nut3Name: String,
        nut4Name: String,
        nut5Name: String,
        nut6Name: String

    })

var Header = mongoose.model('Header',
    {
        arduinoID: Number,
        systemOff: Boolean,
        updateParams: Boolean,
        pH4Calibration: Boolean,
        pH7Calibration: Boolean,
        eCCalibration: Boolean,
        n1PumpOR: Boolean,
        n2PumpOR: Boolean,
        n3PumpOR: Boolean,
        n4PumpOR: Boolean,
        pHDownPumpOR: Boolean,
        optProcedureOR: Boolean
    }
)

var Datalog = mongoose.model('Datalog',
    {
        arduinoID: Number,
        timeStamp: Date,
        milliseconds: Number,
        pH: Number,
        eC: Number,
        temp: Number,
        airTemp: Number,
        humidity: Number,
        co2: Number,
        luminosity: Number,
        instant_pH: Number,
        instant_eC: Number,
        instant_temp: Number,
        arduinoID: Number,
        nut1Prop: Number,
        nut2Prop: Number,
        nut3Prop: Number,
        nut4Prop: Number,
        totalNut: Number,
        pHDownPumpDelay: Number,
        pHSamplingInterval: Number,
        eCSamplingInterval: Number,
        tempSamplingInterval: Number,
        pHDownPumpActInterval: Number,
        nutPumpActInterval: Number,
        optProcedureInterval: Number,
        pHPrecision: Number,
        eCPrecision: Number,
        pHGoal: Number,
        eCGoal: Number,
        humidityGoal: Number,
        airTempGoal: Number,
        circulationTime: Number,
        circPumpState: Boolean,
        nutPumpState: Boolean,
        pHDownPumpState: Boolean,
        optProcedureState: Boolean,
        eCLevelingState: Boolean,
        pHLevelingState: Boolean,
        eCPrecisionState: Boolean,
        pHPrecisionState: Boolean,
        hourOn: Number,
        hourOff: Number,
        minutesOn: Number,
        minutesOff: Number
    }
)

var Message = mongoose.model('Message',
    {
        message: String
    }
)

router.post('/message/:message', function(req, res){
    var message = new Message({})

    message.message = req.param('message')

    message.save(function(err, result){
        if(!err) {
            Message.find(function (err, messages) {
                if (!err) {
                    console.log(messages)
                    res.status(200).send("TODO OK, LOS DATOS FUERON GUARDADOS");
                } else {
                    console.log(err)
                    res.status(500).send(err)
                }
            })
        }
        else {res.status(500).send(err)}
    })
})

router.post('/datalog/', function(req, res){
    var datalog = new Datalog({});
    var postedData = new PostedData({});
    console.log(req.body.timeStamp);
    postedData.str = req.body.timeStamp;
    //var data = JSON.parse(req.body.data);
    //console.log(data)

    datalog.arduinoID = req.body.arduinoID;
    datalog.timeStamp = moment(req.body.timeStamp, 'YYYY-MM-DD-THH:mm:ss')._d;
    datalog.milliseconds = req.body.milliseconds;
    datalog.pH = req.body.pH;
    datalog.eC = req.body.eC;
    datalog.temp = req.body.temp;
    datalog.airTemp = req.body.airTemp;
    datalog.co2 = req.body.co2;
    datalog.humidity = req.body.humidity;
    datalog.luminosity = req.body.luminosity;
    datalog.instant_pH = req.body.instant_pH;
    datalog.instant_eC = req.body.instant_eC;
    datalog.instant_temp = req.body.instant_temp;
    datalog.optProcedureInterval = req.body.optProcedureInterval;
    datalog.nut1Prop = req.body.nut1Prop;
    datalog.nut2Prop = req.body.nut2Prop;
    datalog.nut3Prop = req.body.nut3Prop;
    datalog.nut4Prop = req.body.nut4Prop;
    datalog.totalNut = req.body.totalNut;
    datalog.pHDownML = req.body.pHDownML;
    datalog.nutPumpActInterval = req.body.nutPumpActInterval;
    datalog.pHDownPumpActInterval = req.body.pHDownPumpActInterval;
    datalog.pHSamplingInterval = req.body.pHSamplingInterval;
    datalog.eCSamplingInterval = req.body.eCSamplingInterval;
    datalog.pHPrecision = req.body.pHPrecision;
    datalog.eCPrecision = req.body.eCPrecision;
    datalog.pHGoal = req.body.pHGoal;
    datalog.eCGoal = req.body.eCGoal;
    datalog.humidityGoal = req.body.humidityGoal;
    datalog.airTempGoal = req.body.airTempGoal;
    datalog.tempSamplingInterval = req.body.tempSamplingInterval;
    datalog.circulationTime = req.body.circulationTime;
    datalog.circPumpState = req.body.circPumpState;
    datalog.nutPumpState = req.body.nutPumpState;
    datalog.pHDownPumpState = req.body.pHDownPumpState;
    datalog.optProcedureState = req.body.optProcedureState;
    datalog.eCLevelingState = req.body.eCLevelingState;
    datalog.pHLevelingState = req.body.pHLevelingState;
    datalog.eCPrecisionState = req.body.eCPrecisionState;
    datalog.pHPrecisionState = req.body.pHPrecisionState;
    datalog.hourOn = req.body.hourOn;
    datalog.hourOff = req.body.hourOff;
    datalog.minutesOn = req.body.minutesOn;
    datalog.minutesOff = req.body.minutesOff;

    postedData.save(function(err){
        if(!err){
            console.log("PostedData Save Success");
            datalog.save(function(err){
                if(!err){
                    console.log("datalog Save Success");
                    console.log(datalog);
                    res.status(200).send("{status: OK}");
                } else {
                    res.status(500).send("{status: ERR}");
                }
            })
        } else {
            res.status(500).send("{status: ERR}");
        }
    })

    //datalog.arduinoID = data.arduinoID;
    //datalog.timeStamp = moment(data.timeStamp, 'YYYY-MM-DD-THH:mm:ss')._d;
    //
    //console.log(datalog);
    //postedData.save(function(err){
    //    if(!err){
    //        datalog.save(function(err){
    //            if(!err){
    //                res.status(200).send("{status: OK}");
    //            } else {
    //                console.log(err);
    //                res.status(500).send("{status: ERR}");
    //            }
    //        })
    //    } else {
    //        console.log(err);
    //        res.status(500).send("{status: ERR}");
    //    }
    //})

})

router.get('/get_message', function(req, res){
    res.send("Hola!!! FUCK YOU!!!!");
})

router.get('/test', function(req, res){
    res.send("OK")
})

router.get('/datalog/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}).limit(200).sort('-timeStamp').exec(function(err, datalogs){
        if(!err){
            res.status(200).send(datalogs)
        } else {
            console.log(err)
            res.status(500).send({err: err})
        }
    })
    //Datalog.find({arduinoID: req.params.arduinoID},{limit: 200}, function(err, datalogs){
    //    if(!err){
    //        if(!err){res.status(200).send(datalogs)}
    //        else {res.status(500).send(err)}
    //    } else {
    //        console.log(err)
    //        res.status(500).send({err: err})
    //    }
    //})
})

router.get('/datalog/export/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}, function(err, datalogs){
        var csvData = "";
        if(!err){
            datalogs.forEach(function(row){
                res.send(row.keys());
                console.log(row.keys());
                row.keys().forEach(function(key){
                    csvData += row[key] + ","
                })
                csvData.slice(csvData.length-1, csvData.length);
                csvData += '\n\r';
            })
            res.status(200).csv(csvData);
        } else {
            res.status(500).send(err)
        }
    })
})

router.get('/status/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}).limit(1).sort('-timeStamp').exec(function(err, datalogs){
        if(!err){
            res.status(200).send(datalogs[0])
        } else {
            console.log(err)
            res.status(500).send({err: err})
        }
    })
})

router.get('/datalog/gecko_pH/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}).limit(50).sort('-timeStamp').exec(function(err, datalogs){
        if(!err){
            var xaxis = [];
            var pHValues = [];
            var pHInstantValues = [];
            datalogs.forEach(function(datalog, index){
                xaxis.push(datalog.timeStamp.getMinutes() + ":" + datalog.timeStamp.getSeconds());
                pHValues.push(datalog.pH);
                pHInstantValues.push(datalog.instant_pH);
            })
            var result = {
                chart: {
                    style: {
                        color: "#b9bbbb"
                    },
                    renderTo: "container",
                    backgroundColor: "transparent",
                    lineColor: "rgba(35,37,38,100)",
                    plotShadow: false
                },
                credits: {
                    enabled: false
                },
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "pH Readings"
                },
                xAxis: {
                    categories: xaxis
                },
                yAxis: {
                    title: {
                        style: {
                            color: "#b9bbbb"
                        },
                        text: "pH"
                    }
                },
                legend: {
                    itemStyle: {
                        color: "#b9bbbb"
                    },
                    layout: "vertical",
                    align: "right",
                    verticalAlign: "middle",
                    borderWidth: 0
                },
                series: [{
                    color: "#108ec5",
                    name: "pH",
                    data: pHValues
                }, {
                    color: "#52b238",
                    name: "Instant pH",
                    data: pHInstantValues
                }]
            }
            res.status(200).send(result);
        } else {
            res.status(500).send(err);
        }
    })
})

router.get('/datalog/gecko_temp/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}).limit(50).sort('-timeStamp').exec(function(err, datalogs){
        if(!err){
            var xaxis = [];
            var temp = [];
            var tempInstantValues = [];
            datalogs.forEach(function(datalog, index){
                xaxis.push(datalog.timeStamp.getMinutes() + ":" + datalog.timeStamp.getSeconds());
                temp.push(datalog.temp);
                tempInstantValues.push(datalog.instant_temp);
            })
            var result = {
                chart: {
                    style: {
                        color: "#b9bbbb"
                    },
                    renderTo: "container",
                    backgroundColor: "transparent",
                    lineColor: "rgba(35,37,38,100)",
                    plotShadow: false
                },
                credits: {
                    enabled: false
                },
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "Temperature Readings"
                },
                xAxis: {
                    categories: xaxis
                },
                yAxis: {
                    title: {
                        style: {
                            color: "#b9bbbb"
                        },
                        text: "Temperature"
                    }
                },
                legend: {
                    itemStyle: {
                        color: "#b9bbbb"
                    },
                    layout: "vertical",
                    align: "right",
                    verticalAlign: "middle",
                    borderWidth: 0
                },
                series: [{
                    color: "#108ec5",
                    name: "temp",
                    data: temp
                }, {
                    color: "#52b238",
                    name: "Instant temp",
                    data: tempInstantValues
                }]
            }
            res.status(200).send(result);
        } else {
            res.status(500).send(err);
        }
    })
})

router.get('/datalog/gecko_EC/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}).limit(50).sort('-timeStamp').exec(function(err, datalogs){
        if(!err){
            var xaxis = [];
            var eCValues = [];
            var eCInstantValues = [];
            datalogs.forEach(function(datalog, index){
                xaxis.push(datalog.timeStamp.getMinutes() + ":" + datalog.timeStamp.getSeconds())
                eCValues.push(datalog.eC)
                eCInstantValues.push(datalog.instant_eC);
            })
            var result = {
                chart: {
                    style: {
                        color: "#b9bbbb"
                    },
                    renderTo: "container",
                    backgroundColor: "transparent",
                    lineColor: "rgba(35,37,38,100)",
                    plotShadow: false
                },
                credits: {
                    enabled: false
                },
                title: {
                    style: {
                        color: "#b9bbbb"
                    },
                    text: "EC Readings"
                },
                xAxis: {
                    categories: xaxis
                },
                yAxis: {
                    title: {
                        style: {
                            color: "#b9bbbb"
                        },
                        text: "EC"
                    }
                },
                legend: {
                    itemStyle: {
                        color: "#b9bbbb"
                    },
                    layout: "vertical",
                    align: "right",
                    verticalAlign: "middle",
                    borderWidth: 0
                },
                series: [{
                    color: "#108ec5",
                    name: "EC",
                    data: eCValues
                }, {
                    color: "#52b238",
                    name: "Instant EC",
                    data: eCInstantValues
                }]
            }
            res.status(200).send(result);
        } else {
            res.status(500).send(err);
        }
    })
})

router.get('/messages', function(req, res){
    Message.find(function(err, messages){
        if(!err){
            console.log(messages)
            res.status(200).send(messages)
        } else {
            res.status(500).send(err)
        }
    })
})

router.get('/removeAll/:arduinoID', function(req, res){
    Datalog.find({arduinoID: req.params.arduinoID}).remove(function(err){
        if(!err){
            res.send("All removed ok");
        } else {
            res.status(500).send({err: err})
        }
    })

})

router.get('/header/:arduinoID', function(req, res){
    Header.find({arduinoID: req.params.arduinoID}, function(err, header){
        if(!err){
            var dbHeader = header[0];
            console.log(dbHeader.nut1PumpOR);
            if(req.header('User-Agent') == "Arduino"){
                if(header[0].nut1PumpOR == true){
                    header[0].nut1PumpOR = false;
                }
                header[0].save(function(err){
                    if(!err){
                        res.status(200).send(dbHeader);
                    } else {
                        res.status(500).send('{status: FAIL, error: ' + err + '}');
                    }
                })
            } else {
                res.status(200).send(header[0]);
            }
        } else {
            console.log(err);
            res.status(500).send('{status: FAIL, error: ' + err + '}');
        }
    })

})

router.post('/header/:arduinoID', function(req, res){
    console.log(req.body);
    Header.find({arduinoID: req.params.arduinoID}, function(err, headers){
        if(!err){
            if(headers.length > 0){
                _.extend(headers[0], req.body);
                console.log(headers[0]);
                headers[0].save(function(err){
                    if(!err){
                        if(req.header('User-Agent') == "Arduino"){
                            req.status(200).type("text").send(headers[0].toString());
                        } else {
                            res.status(200).send(headers[0]);
                        }
                    } else {
                        res.status(500).send(err);
                    }
                })
            } else {
                var header = new Header(req.body);
                header.arduinoID = req.params.arduinoID;
                header.save(function(err){
                    if(!err){
                        if(req.header('User-Agent') == "Arduino") {
                            req.status(200).type("text").send(header.toString());
                        } else {
                            res.status(200).send(header);
                        }
                    } else {
                        res.status(500).send(err);
                    }
                })
            }

        } else {res.status(500).send(err);}
    })
})

router.get('/params/:arduinoID', function(req, res){
    Params.find({arduinoID: req.params.arduinoID}, function(err, params){
        if(!err){
            if(req.header('User-Agent') == "Arduino"){
                Header.find({arduinoID: req.params.arduinoID}, function(err, headers){
                    headers[0].updateParams = false;
                    headers[0].save(function(err){
                        if(!err){
                            res.status(200).send(params[0])
                        } else {
                            res.status(500).send(err);
                        }
                    })
                })
            } else {
                console.log(params);
                res.status(200).send(params[0])
            }
        } else {res.status(500).send(err)}
    })
})

router.post('/params/:arduinoID', function(req, res){
    Params.find({arduinoID: req.params.arduinoID}, function(err, db_params){
        if(!err){
            if(db_params.length == 0){
                console.log("NO PARAMS YET")
                //No params object, create
                var params = new Params(req.body);
                params.arduinoID = req.params.arduinoID;
                console.log(params);
                params.save(function(err){
                    if(!err){
                        res.status(200).send(params)
                    } else {res.status(500).send(err)}
                })
            } else {
                var params = req.body
                _.extend(db_params[0], params)
                console.log(params)
                console.log(db_params[0]);
                db_params[0].save(function(err){
                    Header.find({arduinoID: req.params.arduinoID}, function(err, headers){
                        headers[0].updateParams = true;
                        headers[0].save(function(err){
                            if(!err){res.status(200).send(db_params[0])}
                            else {res.status(500).send(err)}
                        })
                    })
                })
            }
        } else {
            res.status(500).send(err);
        }
    })
})



module.exports = router